package LinearAlgebra;

/**
 * Matrix for doing algebraic stuff
 */
public class Matrix {
    private final int m; // rows
    private final int n; // cols
    private float[][] field;

    /**
     * Creates matrix by dimensions.
     * @param m rows
     * @param n cols
     */
    public Matrix(final int m, final int n) {
        this.m = m;
        this.n = n;

        this.field = new float[m][n];
    }

    /**
     * Creates matrix from 2d-array of floats.
     * @param field 2d-array of floats
     */
    public Matrix(final float[][] field) {
        this.m = field.length;
        this.n = field[0].length;

        this.field = field;
    }

    /**
     * Create matrix from vector.
     * @param v vector v
     */
    public Matrix(final Vector v) {
        this.m = v.getLength();
        this.n = 1;
        this.field = new float[m][n];

        for (int i = 0; i < this.m; i++) {
            for (int j = 0; j < this.n; j++) {
                this.field[i][j] = v.getValue(i);
            }
        }
    }

    // Getter
    /**
     * Returns number of rows of this matrix.
     * @return number of rows
     */
    public int getM() {
        return this.m;
    }

    /**
     * Returns number of columns of this matrix.
     * @return number of columns
     */
    public int getN() {
        return this.n;
    }

    /**
     * Returns the value at the desired position in the matrix.
     * @param mPos row of the desired value
     * @param nPos column of the desired value
     * @return the desired value
     */
    public float getValue(final int mPos, final int nPos) {
        return this.field[mPos][nPos];
    }
    //----------

    // Setter
    /**
     * Sets the value at the desired position to the desired value
     * @param mPos row of the desired value
     * @param nPos column of the desired value
     * @param value desired value to set
     */
    public void setValue(final int mPos, final int nPos, final float value) {
        this.field[mPos][nPos] = value;
    }
    //----------

    // Operations
    /**
     * Adds matrix a to this matrix.
     * @param a matrix a
     */
    public void add(Matrix a) {
        if ((this.getM() != a.getM()) || (this.getN() != a.getN()))
            throw new RuntimeException("Dimension of the matrices don't match.");

        float newValue;
        for (int i = 0; i < a.getM(); i++)
            for (int j = 0; j < a.getN(); j++) {
                newValue = this.getValue(i, j) + a.getValue(i, j);
                this.setValue(i, j, newValue);
            }
    }

    public void add(float x) {
        for (int i = 0; i < getM(); i++)
            for (int j = 0; j < getN(); j++) {
                this.setValue(i, j, getValue(i, j) + x);
            }
    }

    /**
     * Subtracts matrix b from this matrix.
     * @param a matrix a
     */
    public void subtract(Matrix a) {
        if ((this.getM() != a.getM()) || (this.getN() != a.getN()))
            throw new RuntimeException("Dimension of the matrices don't match.");

        float newValue;

        for (int i = 0; i < getM(); i++)
            for (int j = 0; j < getN(); j++) {
                newValue = this.getValue(i, j) - a.getValue(i, j);
                this.setValue(i, j, newValue);
            }
    }

    /**
     * Multiplies this matrix by the scalar.
     * @param scalar scalar
     */
    public void multiplyByScalar(float scalar) {
        float newValue;

        for (int i = 0; i < getM(); i++)
            for (int j = 0; j < getN(); j++) {
                newValue = this.getValue(i, j) * scalar;
                this.setValue(i, j, newValue);
            }
    }

    /**
     * Multiplies this matrix by another matrix.
     * @param a matrix a
     * @return the resulting matrix
     */
    public Matrix multiplyByMatrix(Matrix a) {
        int m1 = getM();
        int n1 = getN();
        int m2 = a.getM();
        int n2 = a.getN();

        if (n1 != m2)
            throw new RuntimeException("Matrix A has " + n1 + " rows, but Matrix B has " + m2 + " columns.");

        Matrix result = new Matrix(m1, n2);
        float newValue = 0;

        for (int i = 0; i < m1; i++) {
            for (int j = 0; j < n2; j++) {
                for (int k = 0; k < n1; k++) {
                    newValue += this.getValue(i, k) * a.getValue(k, j);
                    result.setValue(i, j, newValue);
                }
                newValue = 0;
            }
        }

        return result;
    }

    /**
     * Hadamard Product: element-wise multiplication of two matrices
     * @param a matrix a
     * @return element-wise multiplication of this matrix and matrix a
     */
    public Matrix hadamardProduct(Matrix a) {
        int m1 = getM();
        int n1 = getN();
        int m2 = a.getM();
        int n2 = a.getN();

        if ((m1 != m2) || (n1 != n2))
            throw new RuntimeException("Matrices don't have matching dimensions.");

        Matrix result = new Matrix(m1, n1);
        float newValue;

        for (int i = 0; i < m1; i++) {
            for (int j = 0; j < n1; j++) {
                newValue = this.getValue(i, j) * a.getValue(i, j);
                result.setValue(i, j, newValue);
            }
        }

        return result;
    }

    /**
     * Transposes this matrix.
     * @return transposed matrix
     */
    public Matrix transpose() {
        Matrix result = new Matrix(this.getN(), this.getM());

        for (int i = 0; i < getM(); i++) {
            for (int j = 0; j < getN(); j++) {
                result.setValue(j, i, this.getValue(i, j));
            }
        }

        return result;
    }

    /**
     * Matrix-Vector multiplication.
     * @param v vector v
     * @return product of this matrix and vector b in a form of a vector.
    */
    public Vector multiply(Vector v) {
        int m = getM();
        int n = getN();

        if (v.getLength() != n)
            throw new RuntimeException("Illegal matrix dimensions.");

        Vector result = new Vector(m);

        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
            result.setValue(i, result.getValue(i) + getValue(i,j) * v.getValue(j));

        return result;
    }
    //----------

    /**
     * Prints this matrix in its form
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < this.m; i++) {
            for (int j = 0; j < this.n; j++) {
                builder.append(this.field[i][j]);
                builder.append(" ");
            }
            builder.append("\n");
        }
        builder.append("\n");

        return builder.toString();
    }
}
