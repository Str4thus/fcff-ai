package LinearAlgebra;

/**
 * Vector of doing algebra stuff.
 */
public class Vector {
    private final int length;
    private float[] field;

    /**
     * Constructor for a column vector.
     * @param height number of rows
     */
    public Vector(final int height) {
        this.length = height;
        this.field = new float[height];
    }

    /**
     * Constructs a vector from an already existing array. (Column Vector)
     * @param field already existing array
     */
    public Vector(final float[] field) {
        this.field = field;
        this.length = field.length;
    }

    // Getter
    /**
     * Returns the length of the vector.
     * @return length of the vector
     */
    public int getLength() {
        return length;
    }

    /**
     * Returns the value in the array at index "index".
     * @param index index of the value in the array
     * @return desired value
     */
    public float getValue(final int index) {
        return this.field[index];
    }
    //----------

    // Setter
    /**
     * Changes the value at index "index" to value "value".
     * @param index index of the value to change
     * @param value value that is being set
     */
    public void setValue(final int index, final float value) {
        this.field[index] = value;
    }
    //----------

    // Operations
    /**
     * Adds vector v to this vector
     * @param v vector v
     * @return sum of this vector and vector v
     */
    public Vector add(Vector v) {
        if (v.getLength() != this.getLength())
            throw new IllegalArgumentException("Length of vectors don't match");

        Vector result = new Vector(getLength());

        for (int i = 0; i < getLength(); i++) {
            result.setValue(i, this.getValue(i) + v.getValue(i));
        }

        return result;
    }

    /**
     * Subtracts vector v from this vector
     * @param v vector v
     * @return difference of this vector and vector v
     */
    public Vector subtract(Vector v) {
        if (v.getLength() != this.getLength())
            throw new IllegalArgumentException("Length of vectors don't match");

        Vector result = new Vector(getLength());

        for (int i = 0; i < getLength(); i++) {
            result.setValue(i, this.getValue(i) - v.getValue(i));
        }

        return result;
    }

    /**
     * Scales the vector by scalar x
     * @param x scalar x
     * @return scaled vector
     */
    public Vector scale(float x) {
        Vector result = new Vector(getLength());

        for (int i = 0; i < getLength(); i++) {
            result.setValue(i, this.getValue(i) * x);
        }

        return result;
    }

    /**
     * Dot product of this vector and vector v.
     * @param v vector v
     * @return scalar, the result of the dot product
     */
    public float dot(Vector v) {
        if (v.getLength() != this.getLength())
            throw new IllegalArgumentException("Length of vectors don't. Vector A's length: " + this.getLength()
                    + "; Vector B's length: " + v.getLength());

        float result = 0;

        for (int i = 0; i < getLength(); i++) {
            result += getValue(i) * v.getValue(i);
        }

        return result;
    }

    /**
     * Cross product of this vector and vector v.
     * @param v vector v
     * @return cross product in form of a vector
     */
    public Vector cross(Vector v) {
        if (v.getLength() != this.getLength())
            throw new IllegalArgumentException("Length of vectors. Vector A's length: " + this.getLength()
                    + "; Vector B's length: " + v.getLength());

        Vector result = new Vector(getLength());

        for (int i = 0; i < getLength(); i++) {
            int currBuf =  (i+1) % getLength();
            int nextBuf = (currBuf + 1) % getLength();

            result.setValue(i, (this.getValue(currBuf) * v.getValue(nextBuf)) - (this.getValue(nextBuf) * v.getValue(currBuf)));
        }

        return result;
    }
    //----------

    /**
     * Prints the vector.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < getLength(); i++) {
            builder.append(getValue(i));
            builder.append("\n");
        }

        return builder.toString();
    }
}
