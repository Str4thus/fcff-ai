package NeuralNetwork.Layer;

import LinearAlgebra.Vector;
import NeuralNetwork.Neuron.HiddenNeuron;
import NeuralNetwork.Neuron.InputNeuron;
import NeuralNetwork.Neuron.Neuron;
import NeuralNetwork.Neuron.OutputNeuron;

/**
 * Abstract class.
 * A Layer consists of neurons.
 */
public abstract class Layer {
    Vector outputValues;
    private int id;

    private Neuron[] neurons;

    enum Type {
        Input, Hidden, Output
    }

    Layer(final int size, final int id, final Type type) {
        this.id = id;
        this.neurons = new Neuron[size];

        switch (type) {
            case Input:
                fillLayerWithInputNeurons();
                break;

            case Hidden:
                fillLayerWithHiddenNeurons();
                break;

            case Output:
                fillLayerWithOutputNeurons();
                break;
        }
    }


    private void fillLayerWithInputNeurons() {
        for (int i = 0; i < neurons.length; i++) {
            neurons[i] = new InputNeuron();
        }
    }

    private void fillLayerWithHiddenNeurons() {
        for (int i = 0; i < neurons.length; i++) {
            neurons[i] = new HiddenNeuron(i);
        }
    }

    private void fillLayerWithOutputNeurons() {
        for (int i = 0; i < neurons.length; i++) {
            neurons[i] = new OutputNeuron(i);
        }
    }



    public Neuron getNeuronAt(final int index) {
        if (index >= neurons.length)
            throw new RuntimeException("Index neuron is out of bounds.");

        return neurons[index];
    }

    public int getNumberOfNeurons() {
        return neurons.length;
    }

    abstract void compute();

    public int getId() {
        return this.id;
    }
}
