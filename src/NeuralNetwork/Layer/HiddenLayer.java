package NeuralNetwork.Layer;

import LinearAlgebra.Matrix;
import LinearAlgebra.Vector;
import NeuralNetwork.Interfaces.Connectable;
import NeuralNetwork.Interfaces.Weightable;
import NeuralNetwork.NeuralNetwork;
import NeuralNetwork.Neuron.Connection;
import NeuralNetwork.Neuron.HiddenNeuron;
import NeuralNetwork.Neuron.Neuron;
import NeuralNetwork.Neuron.OutputNeuron;

/**
 * The hidden layer consists of hidden neurons.
 */
public class HiddenLayer extends Layer implements Weightable {
    private Layer previousLayer;
    private Layer nextLayer;

    private Matrix weights;
    /*
    w11 w12 w1b
    w21 w22 w2b
     */

    public HiddenLayer(final int size, final int id) {
        super(size, id, Type.Hidden);
    }


    public void setNextLayer(Layer nextLayer) {
        this.nextLayer = nextLayer;
    }


    public void setPreviousLayer(Layer previousLayer) {
        this.previousLayer = previousLayer;
        fullyConnectToPreviousLayer();
    }

    @Override
    public void compute() {
        Vector input = previousLayer.outputValues;
        Vector output = new Vector(getNumberOfNeurons()+1);

        for (int i = 0; i < getNumberOfNeurons(); i++) {
            ((HiddenNeuron) getNeuronAt(i)).process(input);

            output.setValue(i, getNeuronAt(i).getActivationValue());
        }

        output.setValue(getNumberOfNeurons(), 1);
        outputValues = output;
    }

    @Override
    public void adjustWeights() {
        createWeightMatrix();
        Matrix deltaWeights = new Matrix(getNumberOfNeurons(), previousLayer.getNumberOfNeurons()+1);

        for (int i = 0; i < deltaWeights.getM(); i++) {
            for (int j = 0; j < deltaWeights.getN(); j++) {
                Connection c = ((HiddenNeuron)getNeuronAt(i)).getConnection(j);
                float deltaWeight = NeuralNetwork.learningRate * ((HiddenNeuron) getNeuronAt(i)).getErrorSignal() * c.otherNeuron.getActivationValue();

                deltaWeights.setValue(i, j, deltaWeight);
            }
        }

        weights.add(deltaWeights);

        for (int i = 0; i < deltaWeights.getM(); i++) {
            for (int j = 0; j < deltaWeights.getN(); j++) {
                Connection c = ((HiddenNeuron)getNeuronAt(i)).getConnection(j);
                c.weight = weights.getValue(i, j);
            }
        }
    }

    private void createWeightMatrix() {
        weights = new Matrix(getNumberOfNeurons(), previousLayer.getNumberOfNeurons()+1);

        for (int i = 0; i < getNumberOfNeurons(); i++) {
            for (int j = 0; j < previousLayer.getNumberOfNeurons()+1; j++) {
                Connection c = ((Connectable) getNeuronAt(i)).getConnection(j);

                weights.setValue(i, j, c.weight);
            }
        }
    }

    private void fullyConnectToPreviousLayer() {
        int sizeOfPreviousLayer = previousLayer.getNumberOfNeurons();

        for (int i = 0; i < getNumberOfNeurons(); i++) {
            Connection[] connections = new Connection[sizeOfPreviousLayer + 1]; //+1 for bias

            for (int j = 0; j < sizeOfPreviousLayer; j++) {
                connections[j] = new Connection(previousLayer.getNeuronAt(j));
            }
            connections[sizeOfPreviousLayer] = new Connection(NeuralNetwork.getBias());

            ((HiddenNeuron) getNeuronAt(i)).setConnections(connections);
        }
    }
}
