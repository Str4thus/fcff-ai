package NeuralNetwork.Layer;

import LinearAlgebra.Matrix;
import LinearAlgebra.Vector;
import NeuralNetwork.Interfaces.Connectable;
import NeuralNetwork.Interfaces.Weightable;
import NeuralNetwork.NeuralNetwork;
import NeuralNetwork.Neuron.Connection;
import NeuralNetwork.Neuron.OutputNeuron;

public class OutputLayer extends Layer implements Weightable {
    private Layer nextLayer;
    private Layer previousLayer;

    private Matrix weights;


    public OutputLayer(final int size, final int id) {
        super(size, id, Type.Output);
    }

    private void createWeightMatrix() {
        weights = new Matrix(getNumberOfNeurons(), previousLayer.getNumberOfNeurons()+1);

        for (int i = 0; i < getNumberOfNeurons(); i++) {
            for (int j = 0; j < previousLayer.getNumberOfNeurons()+1; j++) {
                Connection c = ((Connectable) getNeuronAt(i)).getConnection(j);

                weights.setValue(i, j, c.weight);
            }
        }
    }


    public void setNextLayer(Layer nextLayer) {
        this.nextLayer = nextLayer;
    }


    public void setPreviousLayer(Layer previousLayer) {
        this.previousLayer = previousLayer;
        fullyConnectToPreviousLayer();
    }

    private void fullyConnectToPreviousLayer() {
        int sizeOfPreviousLayer = previousLayer.getNumberOfNeurons();

        for (int i = 0; i < getNumberOfNeurons(); i++) {
            Connection[] connections = new Connection[sizeOfPreviousLayer + 1]; //+1 for bias

            for (int j = 0; j < sizeOfPreviousLayer; j++) {
                connections[j] = new Connection(previousLayer.getNeuronAt(j));
            }
            connections[sizeOfPreviousLayer] = new Connection(NeuralNetwork.getBias());

            ((OutputNeuron) getNeuronAt(i)).setConnections(connections);
        }
    }

    public void setTarget(Vector target) {
        for (int i = 0; i < target.getLength(); i++) {
            ((OutputNeuron) getNeuronAt(i)).setTarget(target.getValue(i));
        }
    }

    @Override
    public void compute() {
        Vector input = previousLayer.outputValues;
        Vector output = new Vector(getNumberOfNeurons());

        for (int i = 0; i < getNumberOfNeurons(); i++) {
            ((OutputNeuron) getNeuronAt(i)).process(input);

            output.setValue(i, getNeuronAt(i).getActivationValue());
        }

        outputValues = output;
    }

    public Vector getOutput() {
        return this.outputValues;
    }

    @Override
    public void adjustWeights() {
        createWeightMatrix();
        Matrix deltaWeights = new Matrix(getNumberOfNeurons(), previousLayer.getNumberOfNeurons()+1);

        for (int i = 0; i < deltaWeights.getM(); i++) {
            for (int j = 0; j < deltaWeights.getN(); j++) {
                Connection c = ((OutputNeuron)getNeuronAt(i)).getConnection(j);
                float deltaWeight = NeuralNetwork.learningRate * ((OutputNeuron) getNeuronAt(i)).getErrorSignal() * c.otherNeuron.getActivationValue();

                deltaWeights.setValue(i, j, deltaWeight);
            }
        }

        weights.add(deltaWeights);

        for (int i = 0; i < deltaWeights.getM(); i++) {
            for (int j = 0; j < deltaWeights.getN(); j++) {
                Connection c = ((OutputNeuron)getNeuronAt(i)).getConnection(j);
                c.weight = weights.getValue(i, j);
            }
        }
    }




    /*
    public void setTarget(Vector target) {
        this.target = target;
    }

    @Override
    public void compute() {
        inputValues = new Vector(previousLayer.getNumberOfNeurons()+1);
        outputValues = new Vector(getNumberOfNeurons());

        inputValues.setValue(0, NeuralNetwork.bias.activate());
        for (int i = 1; i < inputValues.getLength(); i++) {
            inputValues.setValue(i, previousLayer.getNeuronAt(i-1).activate());
        }

        for (int i = 0; i < getNumberOfNeurons(); i++) {
            outputValues.setValue(i, getNeuronAt(i).activate());
        }
    }

    @Override
    public Vector getErrorVector() {
        return errorVector;
    }

    @Override
    public void createErrorVector() {
        for (int i = 0; i < errorVector.getLength(); i++) {
            ((OutputNeuron) getNeuronAt(i)).calculateErrorSignal(getNeuronAt(i).activate(), target.getValue(i));
            float x = ((OutputNeuron) getNeuronAt(i)).getErrorSignal();
            errorVector.setValue(i, x);
        }
    }



    private void createWeightMatrix() {
        weights = new Matrix(getNumberOfNeurons(), previousLayer.getNumberOfNeurons()+1);

        for (int i = 0; i < weights.getM(); i++) {
            Neuron currentNeuronInThisLayer = getNeuronAt(i);

            for (int j = 0; j < weights.getN()-1; j++) {
                weights.setValue(i, j, ((Connectable)currentNeuronInThisLayer).getWeightFromConnection(j));
            }
        }
    }

    private void fullyConnectToPreviousLayer() {
        for (int i = 0; i < getNumberOfNeurons(); i++) {
            Connection[] connections = new Connection[previousLayer.getNumberOfNeurons()+1];
            connections[0] = new Connection(NeuralNetwork.bias); // Always connect to the bias!

            for (int j = 1; j < getNumberOfNeurons()+2; j++) {
                connections[j] = new Connection(previousLayer.getNeuronAt(j-1));
            }

            OutputNeuron on = (OutputNeuron) getNeuronAt(i);
            on.setConnections(connections);
        }
    }

    public Vector getOutput() {
        return outputValues;
    }
    */
}
