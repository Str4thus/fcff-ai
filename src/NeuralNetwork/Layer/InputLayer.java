package NeuralNetwork.Layer;

import LinearAlgebra.Vector;
import NeuralNetwork.Neuron.InputNeuron;

public class InputLayer extends Layer {
    private Vector inputValues;
    private Layer nextLayer;

    public InputLayer(final int size) {
        super(size, 0, Type.Input);
    }

    public void setInputValues(Vector inputValues) {
        if (inputValues.getLength() != getNumberOfNeurons())
            throw new RuntimeException("Number of inputs must match number of input neurons!");

        this.inputValues = inputValues;

        for (int i = 0; i < inputValues.getLength(); i++) {
            ((InputNeuron) getNeuronAt(i)).setInputValue(inputValues.getValue(i));
        }
        compute();
    }


    public void setNextLayer(Layer layer) {
        this.nextLayer = layer;
    }


    public void setPreviousLayer(Layer layer) {
        // Input Layers don't have a previous layer
    }

    @Override
    public void compute() {
        Vector output = new Vector(getNumberOfNeurons()+1);

        for (int i = 0; i < inputValues.getLength(); i++) {
            output.setValue(i, getNeuronAt(i).getActivationValue());
        }
        output.setValue(getNumberOfNeurons(), 1);
        this.outputValues = output;
    }
}
