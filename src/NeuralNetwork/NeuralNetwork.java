package NeuralNetwork;

import LinearAlgebra.Vector;
import NeuralNetwork.Interfaces.Weightable;
import NeuralNetwork.Layer.HiddenLayer;
import NeuralNetwork.Layer.InputLayer;
import NeuralNetwork.Layer.Layer;
import NeuralNetwork.Layer.OutputLayer;
import NeuralNetwork.Neuron.Bias;
import NeuralNetwork.Neuron.HiddenNeuron;
import NeuralNetwork.Neuron.Neuron;

/**
 * This is where the magic comes together.
 */
public class NeuralNetwork {
    public final static float learningRate = 4f;
    static Bias bias; // The bias, where all non-input neurons are connected to.
    private Layer[] layers;

    public NeuralNetwork(final int iSize, final int hSize, final int oSize, final int numberOfLayers) {
        bias = new Bias();

        layers = new Layer[numberOfLayers];

        // Create layers
        for (int i = 0; i < numberOfLayers; i++) {
            if (i == 0)
                layers[i] = new InputLayer(iSize);
            else if (i == numberOfLayers-1)
                layers[i] = new OutputLayer(oSize, i);
            else
                layers[i] = new HiddenLayer(hSize, i);
        }

        // Connect layers
        for (int i = 1; i < layers.length-1; i++) {
            HiddenLayer currentLayer = ((HiddenLayer) layers[i]);

            currentLayer.setPreviousLayer(layers[currentLayer.getId() - 1]);
            currentLayer.setNextLayer(layers[currentLayer.getId() + 1]);

            for (int j = 0; j < currentLayer.getNumberOfNeurons(); j++) {
                Neuron[] neurons = new Neuron[layers[currentLayer.getId()+1].getNumberOfNeurons()];

                for (int k = 0; k < layers[currentLayer.getId()+1].getNumberOfNeurons(); k++) {
                    neurons[k] = layers[currentLayer.getId()+1].getNeuronAt(k);
                }

                ((HiddenNeuron) currentLayer.getNeuronAt(j)).setNextLayerNeurons(neurons);
            }
        }

        ((OutputLayer) layers[layers.length-1]).setPreviousLayer(layers[layers.length-2]);
    }

    public static Bias getBias() {
        return bias;
    }

    public Vector predict(Vector input) {
        InputLayer inputLayer = (InputLayer) layers[0];
        OutputLayer outputLayer = (OutputLayer) layers[layers.length-1];

        inputLayer.setInputValues(input);

        for (int i = 1; i < layers.length-1; i++) {
            HiddenLayer hiddenLayer = (HiddenLayer) layers[i];
            hiddenLayer.compute();
        }

        outputLayer.compute();
        return outputLayer.getOutput();
    }

    public void train(Vector input, Vector target) {
        OutputLayer outputLayer = (OutputLayer) layers[layers.length-1];
        outputLayer.setTarget(target);

        predict(input);

        for (int i = 1; i < layers.length; i++) {
            ((Weightable) layers[i]).adjustWeights();
        }
    }

}
