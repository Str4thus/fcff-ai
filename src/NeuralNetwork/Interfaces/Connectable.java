package NeuralNetwork.Interfaces;

import NeuralNetwork.Neuron.Connection;

public interface Connectable {
    Connection getConnection(final int numberOfConnection);
    void setConnections(final Connection[] connections);
}
