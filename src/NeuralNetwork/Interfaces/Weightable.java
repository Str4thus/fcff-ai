package NeuralNetwork.Interfaces;

import LinearAlgebra.Vector;

public interface Weightable {

    /*
    Vector getErrorVector();
    void createErrorVector();
    */
    void adjustWeights();

}
