package NeuralNetwork.Interfaces;

public interface Guilty {

    void calculateErrorSignal();
    float getErrorSignal();
}
