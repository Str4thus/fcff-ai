package NeuralNetwork.Neuron;

import java.util.concurrent.ThreadLocalRandom;

/**
 * A connection connects two neurons. It has a weight, which can be adjusted and it stores the neuron, which the connection
 * leads to.
 */
public class Connection {
    public float weight = ThreadLocalRandom.current().nextInt(-1,2);
    public Neuron otherNeuron;

    public Connection(Neuron otherNeron) {
        this.otherNeuron = otherNeron;
    }
}
