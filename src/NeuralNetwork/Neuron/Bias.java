package NeuralNetwork.Neuron;

import LinearAlgebra.Vector;

/**
 * A bias has the output value of "1". A neuron which is not in the input layer has to be connected to the bias
 */
public class Bias extends Neuron {
    private final float output = 1f;

    /**
     * Creates a bias
     */
    public Bias() {
        process(new Vector(0));
        calculateActivationValue();
        calculateWeightedSum(new Vector(3));
    }

    @Override
    void process(Vector input) {
        calculateActivationValue();
    }

    @Override
    void calculateActivationValue() {
        setActivationValue(output);
    }

    @Override
    void calculateWeightedSum(Vector input) {
        setWeightedSum(1f);
    }
}
