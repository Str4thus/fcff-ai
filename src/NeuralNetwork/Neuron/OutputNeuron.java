package NeuralNetwork.Neuron;

import LinearAlgebra.Vector;
import NeuralNetwork.Interfaces.Connectable;
import NeuralNetwork.Interfaces.Guilty;

public class OutputNeuron extends Neuron implements Connectable, Guilty {
    private Connection[] connections;
    private float errorSignal;

    private float target;

    private int id;

    public OutputNeuron(final int id) {
        this.id = id;
    }

    @Override
    public void calculateErrorSignal() {
        errorSignal = dsigmoid(getWeightedSum()) * (target - getActivationValue());
    }

    @Override
    public float getErrorSignal() {
        calculateErrorSignal();
        return errorSignal;
    }

    public void setTarget(float target) {
        this.target = target;
    }

    @Override
    public Connection getConnection(final int numberOfConnection) {
        return connections[numberOfConnection];
    }

    @Override
    public void setConnections(Connection[] connections) {
        this.connections = connections;
    }

    @Override
    void calculateWeightedSum(Vector input) {
        float weightedSum = 0;

        for (int i = 0; i < input.getLength(); i++) {
            weightedSum += input.getValue(i) * connections[i].weight;
        }

        setWeightedSum(weightedSum);
    }

    @Override
    void calculateActivationValue() {
        setActivationValue(sigmoid(getWeightedSum()));
    }


    @Override
    public void process(Vector input) {
        calculateWeightedSum(input);
        calculateActivationValue();
    }
}
