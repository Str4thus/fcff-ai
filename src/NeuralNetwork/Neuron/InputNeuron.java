package NeuralNetwork.Neuron;

import LinearAlgebra.Vector;

public class InputNeuron extends Neuron {
    private float outputValue;

    public InputNeuron(){}

    public void setInputValue(float inputValue) {
        this.outputValue = inputValue;
        float[]inputArray = {inputValue};

        Vector input = new Vector(inputArray);
        process(input);
    }

    @Override
    void calculateWeightedSum(Vector input) {
        // Doesn't need to calculate weighted sum
    }

    @Override
    void calculateActivationValue() {
        setActivationValue(outputValue);
    }

    @Override
    void process(Vector input) {
        calculateActivationValue();
    }
}
