package NeuralNetwork.Neuron;

import LinearAlgebra.Vector;

/**
 * Abstract class.
 * A Neuron has connections to other neurons. The Connections are weighted, which means, some connections are stronger
 * than others. The weights are being multiplied with the input coming via the connection. The sum of all those values is
 * the weighted sum. The weighted sum gets passed into the activation function, which produces an activation value. This value
 * is the output of this neuron which gets transmitted via another connections to other neurons and serves as a input to them.
 */
public abstract class Neuron {
    private float activationValue; // Output
    private float weightedSum; // Input

    /**
     * Activation function
     * @param x function argument
     * @return function value
     */
    static float sigmoid(final float x) {
        return (float) (1 / (1 + Math.exp(-x) ));
    }

    /**
     * Derivative of the activation function
     * @param x function argument
     * @return function value
     */
    static float dsigmoid(final float x) {
        return (sigmoid(x) * (1 - sigmoid(x)));
    }

    /**
     * Sets the weighted sum of the neuron
     * @param weightedSum weighted sum
     */
    void setWeightedSum(float weightedSum) {
        this.weightedSum = weightedSum;
    }


    /**
     * Returns the weighted sum of the neuron
     * @return weighted sum
     */
    float getWeightedSum() {
        return weightedSum;
    }

    /**
     * Sets the activation value
     * @param activationValue activation value
     */
    void setActivationValue(final float activationValue) {
        this.activationValue = activationValue;
    }

    /**
     * Returns the activation value of this neuron
     * @return the activation value of this neuron
     */
    public float getActivationValue() {
        return activationValue;
    }

    // Abstract methods
    /**
     * Calculates the weighted sum based on the inputs to this neuron
     * @param input input to this neuron
     */
    abstract void calculateWeightedSum(Vector input);

    /**
     * Calculates the activation value based on the weighted sum of this neuron.
     */
    abstract void calculateActivationValue();

    /**
     * Processes the input to this neuron
     * @param input input to this neuron
     */
    abstract void process(Vector input);
}
