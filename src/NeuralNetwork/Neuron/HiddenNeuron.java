package NeuralNetwork.Neuron;

import LinearAlgebra.Vector;
import NeuralNetwork.Interfaces.Connectable;
import NeuralNetwork.Interfaces.Guilty;

public class HiddenNeuron extends Neuron implements Connectable, Guilty {
    private Connection[] connections;
    private Neuron[] nextLayerNeurons;

    private float errorSignal;
    private int id;

    public HiddenNeuron(final int id) {
        this.id = id;
    }

    @Override
    public void calculateErrorSignal() {
        float errorValue = 0;

        for (int i = 0; i < nextLayerNeurons.length; i++) {
            errorValue += ((Guilty) nextLayerNeurons[i]).getErrorSignal() * ((Connectable) nextLayerNeurons[i]).getConnection(id).weight;
        }

        errorSignal = errorValue * dsigmoid(getWeightedSum());
    }

    public void setNextLayerNeurons(Neuron[] nextLayerNeurons) {
        this.nextLayerNeurons = nextLayerNeurons;
    }

    @Override
    public float getErrorSignal() {
        calculateErrorSignal();
        return errorSignal;
    }

    @Override
    void calculateWeightedSum(Vector input) {
        float weightedSum = 0;

        for (int i = 0; i < input.getLength(); i++) {
            weightedSum += input.getValue(i) * connections[i].weight;
        }

        setWeightedSum(weightedSum);
    }

    @Override
    void calculateActivationValue() {
        setActivationValue(sigmoid(getWeightedSum()));
    }

    @Override
    public Connection getConnection(final int numberOfConnection) {
        return connections[numberOfConnection];
    }

    @Override
    public void setConnections(Connection[] connections) {
        this.connections = connections;
    }

    @Override
    public void process(Vector input) {
        calculateWeightedSum(input);
        calculateActivationValue();
    }

    /*
    public void setConnections(Connection[] connections) {
        this.connections = connections;
    }

    public void calculateErrorSignal(Connectable[] neuronsFromNextLayer) {
        this.errorSignal = 0;

        for (int i = 0; i < neuronsFromNextLayer.length; i++) {
            errorSignal += neuronsFromNextLayer[i].getErrorSignal() * neuronsFromNextLayer[i].getWeightFromConnection(getIndexInLayer());
        }

        this.errorSignal *= Neuron.dsigmoid(this.weightedSum);
    }

    public float computeWeightedSum() {
        weightedSum = 0;

        for (int i = 0; i < connections.length-1; i++) {
            weightedSum += connections[i].otherNeuron.activate() * connections[i].weight;
        }

        return weightedSum;
    }

    @Override
    public float getWeightFromConnection(int numberOfConnection) {
        if (numberOfConnection >= connections.length)
            throw new RuntimeException("Indexed connection(" + numberOfConnection + "out of bounds(" + (connections.length-1) +").");

        return connections[numberOfConnection].weight;
    }

    @Override
    public void setWeightFromConnection(int numberOfConnection, float weight) {
        if (numberOfConnection >= connections.length)
            throw new RuntimeException("Indexed connection(" + numberOfConnection + "out of bounds(" + (connections.length-1) +").");

        connections[numberOfConnection].weight = weight;
    }

    @Override
    public float activate() {
        computeWeightedSum();
        return Neuron.sigmoid(weightedSum);
    }

    @Override
    public float getErrorSignal() {
        return this.errorSignal;
    }*/
}
