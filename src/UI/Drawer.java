package UI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class Drawer extends JPanel implements MouseListener, MouseMotionListener {
    private int index;
    private Point[] arr = new Point[100000];

    public Drawer(String name) {
        super();
        index = 0;
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        JFrame fr = new JFrame(name);
        fr.add(this);
        fr.setSize(200, 200);
        setBackground(Color.green);
        fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fr.setVisible(true);

    }
    public void paintComponent(Graphics g) {
        super.paintComponents(g);
        for (int i = 0; i < index - 1; i++)
            g.drawLine(arr[i].x, arr[i].y, arr[i + 1].x, arr[i + 1].y);
    }

    public void mouseDragged(MouseEvent e) {
        arr[index] = new Point(e.getX(), e.getY());
        index++;
        System.out.println(index);
        repaint();
    }

    public void mousePressed(MouseEvent e) {
        arr[index] = new Point(e.getX(), e.getY());
        index++;
        System.out.println(index);
        repaint();
    }

    public void mouseExited(MouseEvent e) {}
    public void mouseClicked(MouseEvent e) {}
    public void mouseEntered(MouseEvent e) {}

    public void mouseReleased(MouseEvent e) {
        arr = new Point[100000];
        index = 0;
    }

    public void mouseMoved(MouseEvent e) {}
}
