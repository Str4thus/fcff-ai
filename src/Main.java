import LinearAlgebra.Vector;
import NeuralNetwork.NeuralNetwork;
import UI.Drawer;


public class Main {
    private static Vector testInput1 = new Vector(new float[]{0,0});
    private static Vector testInput2 = new Vector(new float[]{0,1});
    private static Vector testInput3 = new Vector(new float[]{1,0});
    private static Vector testInput4 = new Vector(new float[]{1,1});


    private static Vector target1 = new Vector(new float[]{0});
    private static Vector target2 = new Vector(new float[]{1});
    private static Vector target3 = new Vector(new float[]{1});
    private static Vector target4 = new Vector(new float[]{0});

    private static Vector[] trainingData = new Vector[]{testInput1, testInput2, testInput3, testInput4};
    private static Vector[] target = new Vector[]{target1, target2, target3, target4};

    public static void main(String[] args) {
       new Drawer("Boi");
    }

    private static void testOutput() {
        NeuralNetwork nn = new NeuralNetwork(2,4,1,3);

        System.out.println("Truth table of 'XOR': ");
        System.out.println("-----------------------------");

        System.out.println("Before learning: ");
        System.out.println("False and False: " + Math.round(nn.predict(testInput1).getValue(0)));
        System.out.println("False and True:  " + Math.round(nn.predict(testInput2).getValue(0)));
        System.out.println("True and False:  " + Math.round(nn.predict(testInput3).getValue(0)));
        System.out.println("True and True:   " +Math.round(nn.predict(testInput4).getValue(0)));

        for (int i = 0; i < 300; i++)
            for (int j = 0; j < trainingData.length; j++)
                nn.train(trainingData[j], target[j]);



        System.out.println("\nAfter learning: ");
        System.out.println("False and False: " + Math.round(nn.predict(testInput1).getValue(0)));
        System.out.println("False and True:  " + Math.round(nn.predict(testInput2).getValue(0)));
        System.out.println("True and False:  " + Math.round(nn.predict(testInput3).getValue(0)));
        System.out.println("True and True:   " +Math.round(nn.predict(testInput4).getValue(0)));
    }
}
